package nl.janssen.vertx;

/**
 * Created by Janssen_D on 16-3-2017.
 */

import io.vertx.core.AbstractVerticle;
import io.vertx.core.AsyncResult;
import io.vertx.core.Handler;
import io.vertx.core.eventbus.Message;
import io.vertx.core.http.HttpServer;
import io.vertx.core.http.HttpServerOptions;
import io.vertx.core.http.HttpServerRequest;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;
import io.vertx.rx.java.RxHelper;
import org.apache.camel.rx.ReactiveCamel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import rx.Observable;
import rx.Observer;
import rx.Subscriber;


@Component
public class StaticServer extends AbstractVerticle {

    final int[] counter = {0};

    @Autowired
    ReactiveCamel reactiveCamel;

    Observable myObservable;


    @Override
    public void start() throws Exception {
        Router router = Router.router(vertx);
        router.get("/events").handler(ctx -> {

            ctx.response().setChunked(true);

            ctx.response().headers().add("Content-Type", "text/event-stream;charset=UTF-8");
            ctx.response().headers().add("Connection", "keep-alive");

            vertx.eventBus().consumer("events", msg -> {
                ctx.response().write("event: message\ndata: " + msg.body() + "\n\n");
            });
            System.out.println("hatseflats");
        });
        router.get().handler(ctx -> {
            ctx.response().end("KABOOM");
        });

        this.myObservable = reactiveCamel.toObservable("timer:trigger?period=2000", String.class)
                .map(x -> "Strait from the cameel " + counter[0]++);

        vertx.createHttpServer().requestHandler(router::accept).listen(8080);

        RxHelper.toReadStream(myObservable).handler(handler -> {
            vertx.eventBus().publish("events", handler.toString());

        });


    }


}