package nl.janssen.vertx.camel;

import org.apache.camel.CamelContext;
import org.apache.camel.impl.DefaultCamelContext;
import org.apache.camel.rx.ReactiveCamel;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

/**
 * Created by janssendj on 16-3-17.
 */
@Component
public class Camel {

    @Bean
    ReactiveCamel reactiveCamel(){
        CamelContext context = new DefaultCamelContext();
        try {
            context.start();
        } catch (Exception e){
            System.out.println(e);
        }

        ReactiveCamel rx = new ReactiveCamel(context);
        return rx;
    }


}
