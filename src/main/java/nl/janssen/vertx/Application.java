package nl.janssen.vertx;

/**
 * Created by Janssen_D on 16-3-2017.
 */
import io.vertx.core.Vertx;
import org.apache.camel.CamelContext;
import org.apache.camel.impl.DefaultCamelContext;
import org.apache.camel.rx.ReactiveCamel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import javax.annotation.PostConstruct;

@SpringBootApplication
public class Application {


  @Autowired
  private StaticServer staticServer;

  public static void main(String[] args) {

    // This is basically the same example as the web-examples static site example but it's booted using
    // Spring Boot, not Vert.x
    SpringApplication.run(Application.class, args);
  }





  @PostConstruct
  public void deployVerticle() {
    Vertx.vertx().deployVerticle(staticServer);
  }
}